## Summary

In an effort to avoid duplicated content this readiness review will provide
many cross-links to the architecture blueprint at https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/
.

**Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

See https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#summary for high level summary.

**What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

1. Improved click-through on search results for code search
1. Improved latency for code search
1. Overall greater usage of code search

## Architecture

**Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Make sure to include the following: Internal dependencies, ports, encryption, protocols, security policies, etc.**

https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#design-and-implementation-details

**Describe each component of the new feature and enumerate what it does to support customer use cases.**

https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#design-and-implementation-details

**For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

1. New sidekiq indexing worker failure could consume Sidekiq resources. Can be mitigated by ensuring it runs in an appropriate Sidekiq pool (probably the same as Elasticsearch indexing)
1. Zoekt being slow means searches can be slow which means Rails web processes can be consumed by slow responses. Can be mitigated by leveraging timeouts which we already have on web requests.

**If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#sharding-and-replication-strategy

## Operational Risk Assessment

**What are the potential scalability or performance issues that may result with this change?**

Zoekt web or indexing processes need to scale appropriately as we onboard customers. We will start small but ensuring that we have sufficient Zoekt capacity will ensure searches are fast enough. Our [early benchmarking](https://gitlab.com/gitlab-org/gitlab/-/issues/370832#note_1183611955) suggests that Zoekt will be viable at our scale

**List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

1. [Zoekt](https://github.com/sourcegraph/zoekt) will be impacted if it gets too much indexing or search workload from GitLab
1. Search functionality or index latency will be impacted by failures or slowness in Zoekt
1. Zoekt also requires access to GitLab to pull code and will eventually be expanded with an authentication mechanism that will authenticate cloning private repositories.
1. The integration in GitLab is also storing information related to Zoekt nodes and which namespaces are being stored in each node in Postgres so failures in Postgres would lead to indexing and search issues as we wouldn't know how to locate the Zoekt nodes

**Were there any features cut or compromises made to make the feature launch?**

Yes. They are prioritised in
https://gitlab.com/groups/gitlab-org/-/epics/9404 and should be implemented at
the appropriate stage of the rollout. For example
[sharding and replication](https://gitlab.com/gitlab-org/gitlab-build-images/-/issues/118)
will be implemented after our initial rollout to `gitlab-org` but before it's
rolled out as "beta" to a larger group of customers.

**List the top three operational risks when this feature goes live.**

Given the initial rollout will be very limited nothing stands out as major to
me. We are using feature flags to only roll out to specific users and then
Zoekt indexing will initially only be enabled for a very small group on
GitLab.com and progressively larger groups will be chosen until we get up to
`gitlab-org`. And even then indexing is controlled by a feature flag so can be
easily disabled as well if it somehow caused problems even for small groups.

The only operational risk I perceive is that the Sidekiq indexer worker gets
stuck and consumes Sidekiq resources. This should be easy to fix by disabling
the indexing feature flag.

**What are a few operational concerns that will not be present at launch, but may be a concern later?**

As we scale this out to many customers there will be challenges scaling Zoekt. We will need to closely monitor latency and IO and other Zoekt metrics to ensure that it's scaling. At any point customer impacting search slowness can be mitigated by disabling searching with Zoekt feature flag for specific users or all users and we'll fallback to Elasticsearch.

**Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

Yes there are feature flags for indexing and search. Disabling the indexing
feature flag means we won't queue sidekiq workers to update the index for
Zoekt. Disabling the search feature flag means we'll fallback to using
Elasticsearch for our searches. The flag names are:

1. `index_code_with_zoekt`
1. `search_code_with_zoekt`

It is preferential to disable searching and leave indexing enabled (if
possible) because indexing will get out of sync if it's turned off. Disabling
searching should mitigate any user experience issues so we should only need to
disable indexing if our Sidekiq indexing processes are causing issues.

**Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

1. Code searches from GitLab UI. Failures could result in 500s, slow results or missing results
1. Code searches via the API with the same risks

**As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Worst case examples are described above. If we ignore the already implemented mitigations then the worst case scenario is:

1. Zoekt goes down and all our SREs are spending all their time trying to debug it
   1. This is mitigated by documenting that they can disable a feature flag to
      switch back to Elasticsearch and leave it down until the development team
      determine the cause
1. All our Sidekiq capacity is used up by indexing workers that are stuck
   waiting for Zoekt
   1. This is isolated by using isolated Sidekiq shards for this indexing. And
      quickly mitigated by disabling the feature flag for indexing with Zoekt.
1. All our Puma web workers are used up by waiting for search results from Zoekt.
   1. This is mitigated by disabling the feature flag for searching with Zoekt.
      This is also mitigated by a global 60s timeout on all web requests and
      [our existing search rate limit](https://docs.gitlab.com/ee/administration/instance_limits.html#search-rate-limit).


## Database

**If we use a database, is the data structure verified and vetted by the database team?**

There are some tables stored in Postgres and all of this has gone through the normal Database review. For the most part these models are small scale relative to most of GitLab. Zoekt, however, is itself a new database and will be rolled out slowly with as much feedback as we can get from people that have expertise in databases, storage and Go applications.

**Do we have an approximate growth rate of the stored data (for capacity planning)?**

Detailed projections of storage will be done after small scale rollout and
before we enable to many customers
https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#iterations
. Rough bencharking of memory use and storage has been done in
https://gitlab.com/gitlab-org/gitlab/-/issues/370832 .

In summary when we have enabled for all paying customers it might look like:

1. 60TB of SSD storage (leaving spare capacity)
1. 768GB RAM

**Can we age data and delete data of a certain age?**

Theoretically we may choose to purge projects that haven't been updated in a
while from our index and force them to search via different means. We don't do
this for Elasticsearch today but it's an optimization we could consider. Our
initial implementation will follow the same pattern of Elasticsearch where we
index the default branch for all repositories regardless of age.

## Security and Compliance

See early security review request of blueprint
https://gitlab.com/gitlab-org/gitlab/-/merge_requests/107891#note_1248586481

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [ ] **AWS Accounts/GCP Projects**
  - [ ] **New Subnets**
  - [ ] **VPC/Network Peering**
  - [ ] **DNS names**
  - [ ] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
  - [X] **Other (anything relevant that might be worth mention)**

We are planning on adding an ["internal load balancer"](https://cloud.google.com/kubernetes-engine/docs/how-to/internal-load-balance-ingress)
to allow HTTP traffic between our Kubernetes clusters. This load balancer
should not be internet accessible. This is because Zoekt is stateful and will
run in one cluster and needs to be accessible from all GitLab workloads which
run across multiple clusters. We are assuming that using unencrypted and
unauthenticated traffic here is sufficient considering it's all within our
network but we're open to suggestions of implementing either of these. Since
Zoekt does not itself support terminating SSL, the authentication and/or
encryption would need to happen at an Nginx load balancer in front of Zoekt so
then it would still be unencrypted between Nginx and Zoekt.

- **Secure Software Development Life Cycle (SSDLC)**
    - [ ] **Is the configuration following a security standard? (CIS is a good baseline for example)**
          - I'm not sure
    - [ ] **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**
    - [ ] **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**
    - [ ] **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**
          - Yes it will be managed by bumping the version in our kubernetes-workload repos
    - [ ] **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**
        - [ ] **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**
              - Not using Terraform
  - **If there's a new terraform state:**
    - [ ] **Where is to terraform state stored, and who has access to it?**
          - N/A
  - [ ] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**
  - **If we're creating new containers:**
        - N/A
    - [ ] **Are we using a distroless base image?**
    - **Do we have security scanners covering these containers?**
      - [ ] **`kics` or `checkov` for Dockerfiles for example**
      - [ ] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**
      - The docker images will be managed in https://gitlab.com/gitlab-org/build/CNG so they will have the same scanning we have for all other images that make up our GitLab cloud native helm chart.

- **Identity and Access Management**
  -  [ ] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
         - See https://gitlab.com/gitlab-org/gitlab/-/merge_requests/107891#note_1248586481 . We do need to figure out how Zoekt will authenticate to clone and index private repos from GitLab.
  -  [ ] **Does it follow the least privilege principle?**
         - Priveleges needed will be to clone any repository. We should try to make sure whatever token/auth system we use is limited to this.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  [ ] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**
         - Clones of repositories in GitLab.com as well as `.zoekt` index files that are generated from the repository
  -  [ ] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**
         - Red
  -  [ ] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**
         - The storage will be provided as volume mounts in Kubernetes from GKE (see https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/merge_requests/1 ). Hopefully this is encrypted at rest but Zoekt will not have it's own "at-rest" encryption.
  -  [ ] **Do we have audit logs on data access?**
         - There are many ways we could have "data access" theoerically. Any SRE could access the pod and see the data. I don't know if there is already controls in K8s that audit this. The only way (other than those with K8s access) could access this is via the GitLab application which logs searches.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [ ] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**
         - This should be done using Network Security Policies in Kubernetes to only allow the network traffic needed for this feature to work.
  -  [ ] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**
         - The service is not directly accessible from the internet. It should only be accessible from GitLab and users performing searches. Searches in GitLab already have application limits as well as DDOS protection.
  -  [ ] **Is the service covered by a WAF (Web Application Firewall)**
         - Should we add another WAF in front of this application if it's not internet accessible? It's only accessed via GitLab.

- **Logging & Audit**
  - [ ] **Has effort been made to obscure or elide sensitive customer data in logging?**
        - Logging of GitLab.com project paths may happen now. We could obscure this if we wanted but these are also logged already in GitLab

- **Compliance**
    - [ ] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**
          - I don't know


## Performance

**Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**

See
https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#motivation

**Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

The Postgres database is being used at a very low scale by this feature so it's
unlikely.

**Are there any throttling limits imposed by this feature? If so how are they managed?**

Searches are already throttled in Rails itself.

**If there are throttling limits, what is the customer experience of hitting a limit?**

The same thing they hit today if they use the search feature too often they get
an error.

**For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

Indexing is retried with backoff via Sidekiq. Searches are only retried
manually by a user and not automatically retried. The throttling above limits
the retries to an extent and the "back-off" would probably come from the
psychology of constantly getting errors when performing searches.

**Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

I don't think so. We expect usage of search to grow over time based on
improving the UX but we don't expect this feature to contribute to a surge in
TPS. In fact search usage in GitLab is already quite low relative to overall
feature usage which we're hoping will be improved by improving code search.

## Backup and Restore

**Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

Since Zoekt is a secondary data store then data in Zoekt indexes are duplicates
of data in Gitaly and can be recreated. For our initial small scale rollout we
may choose to not implmement backups in our infrastructure as data loss has
limited capacity to impact users since we can always fallback to Elasticsearch
and then recreate the index and re-enable the feature flags.

We should implement backups of Zoekt disks before large scale rollout as
recreation of Zoekt indexes at scale from Gitaly may be expensive and time
consuming.


**Are backups monitored?**

Ideally we'll implement backup monitoring for whatever does our backups.

**Was a restore from backup tested?**

We should test this when we implement backups.

## Monitoring and Alerts

**Is the service logging in JSON format and are logs forwarded to logstash?**

All logs for now are in JSON format and go to the `sidekiq.log` and
`production_json.log` and will be in JSON format and forwarded to logstash. We plan to [create a separate log file `zoekt.log`](https://gitlab.com/gitlab-org/gitlab/-/issues/389759) that will be JSON formatted and forwarded to logstash.

Our initial deployment of Zoekt will likely log to STDOUT and we should capture
these at least in Stackdriver but we
[plan to implmement structured logging that will be sent to Elasticsearch](https://gitlab.com/gitlab-org/gitlab/-/issues/390001).

**Is the service reporting metrics to Prometheus?**

`zoekt-webserver` and `zoekt-dynamic-indexserver` already exports Prometheus
metrics at `/metrics` so [we should collect those from our monitoring infrastructure](https://gitlab.com/gitlab-org/gitlab/-/issues/390002). It will
also be possible to tell from logs looking at `elasticsearch_*` log entries and
`external_http_*` to determine whether Elasticsearch or Zoekt is being used. We
will also [add specific client side metrics and log entries for Zoekt](https://gitlab.com/gitlab-org/gitlab/-/issues/393450).

Our initial small scale rollout of Zoekt servers may not have Prometheus
metrics, but we'll want to make those available before it's rolled out to any
customers.

**How is the end-to-end customer experience measured?**

Same as existing search metrics. It's a new DB, but experience will be measured
by latency and other web request metrics we have.

**Do we have a target SLA in place for this service?**

Yes we'll keep the same SLA that exists for these search endpoints. These endpoints will already be covered in [The Global Search stage group dashboard](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-global-search-group-dashboard?orgId=1).

**Do we know what the indicators (SLI) are that map to the target SLA?**

Yes we'll keep the same SLI that exists for these search endpoints. These endpoints will already be covered in [The Global Search stage group dashboard](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-global-search-group-dashboard?orgId=1).

**Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Yes we'll keep the same alerts that exists for these search endpoints. These endpoints will already be covered in [The Global Search stage group dashboard](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-global-search-group-dashboard?orgId=1).

**Do we have troubleshooting runbooks linked to these alerts?**

Current runbooks for search issues
[will be updated with Zoekt details](https://gitlab.com/gitlab-org/gitlab/-/issues/390004)
before rolling out to customers.

**What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

During our beta rollout the default for handling an incident should be to
disable the feature flags if customers are impacted. There shouldn't need to be
any tweets.

**do the oncall rotations responsible for this service have access to this service?**

Yes they should be given access to this system the same as they have access to
other parts of our infrastructure. It will run in our normal GCP/K8s
environments they already work in.

## Responsibility

**Which individuals are the subject matter experts and know the most about this feature?**

@DylanGriffith and @dgruzd

**Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

The Global Search team. #g_global_search on Slack

**Is someone from the team who built the feature on call for the launch? If not, why not?**

Yes we will be oncall for launch. We will be launching it during our working
hours using a feature flag.

**How will the system be kept up to date?**

The Global Search team will be responsible for creating merge requests and
staging and production change requests for bumping versions of the Helm chart.
They will not initially have access to the system in production and debugging
may ocassionally require support from an SRE but in the near term we'd like to
implement [Teleport for Zoekt](https://gitlab.com/gitlab-org/gitlab/-/issues/394633)
to give members of the Global Search team more of a self-service (audited) way
to debug staging and production issues.

## Testing

**Describe the load test plan used for this feature. What breaking points were validated?**

Initial benchmarking took place in
https://gitlab.com/gitlab-org/gitlab/-/issues/370832#note_1183611955 . We
didn't reach any breaking points. We plan on gradual rollout in production to
highlight the most likely resources to be starved on Zoekt nodes. Read more in
https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/#motivation

**For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

We haven't tested any failures yet.

**Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

There are end to end and unit tests for the Zoekt integration. The most "end to
end" test indexes repositories and searches for them to confirm the correct
results come back and Zoekt is not mocked out so it actually serves the
results. These will run in our CI pipeline as part of
https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108937 .
