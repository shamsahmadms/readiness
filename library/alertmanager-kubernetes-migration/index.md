# Alertmanager Kubernetes migration

## Problem

We currently run 2 production Alertmanager clusters: one in GCE, that receives
alerts from all GCE-deployed Prometheus instances in all environments (Google
projects, VPCs), and one in gitlab-production project's GKE cluster that
receives alerts from Prometheus instances in that same cluster. In addition to
these, we run Alertmanager instances in non-prod Kubernetes clusters: gstg and
pre.

This setup has a few problems:

1. We must keep config in sync between (at least) the production Alertmanagers,
   or risk behavior inconsistencies.
1. Alerts from GKE Prometheus instances are being sent to a single-node
   Alertmanager cluster. Running a non-HA Alertmanager is quite risky.
1. In the long run, we'd prefer to manage as much infrastructure as possible in
   Kubernetes. Alertmanager is a good candidate for migration.

## Proposed solution

At a high level:

1. Sync up config between the 2 production Alertmanager clusters:
   https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10054.
1. Spin up an Alertmanager cluster in ops GKE.
1. Mesh together the GCE (production and ops) and GKE (production and ops)
   Alertmanagers into one cluster.
1. Ensure some service discovery mechanism exists so that Prometheus instances
   in any VPC, in both GCE and GKE, can discover this single GKE Alertmanager
   cluster. See "Service Discovery" section below.
1. Ensure that Prometheus instances in any VPC, in both GCE and GKE, can reach
   the Alertmanager port on each pod. See "Network Access" section below.
1. Point all non-prod GKE Prometheus instances at the new Alertmanager cluster
   (GKE nodes only).
1. Turn down now-unused non-prod GKE Alertmanager instances (gstg, pre).
1. Point all prod GKE Prometheus instances at the new Alertmanager cluster.
1. Turn down now-unused gprd GKE Alertmanager instances.
1. Point all GCE Prometheus instances in all environments at the new
   Alertmanager cluster (GKE nodes only). We later intend to migrate GCE
   Prometheus jobs onto GKE instances and turn down those GCE instances, but
   that is out of scope for this epic.
1. Point thanos-rule to the new Alertmanager cluster (GKE nodes only).
1. Turn down GCE Alertmanager nodes, now that nothing is pointing at them.

### Service Discovery

Prometheus understands several Service Discovery (SD) schemes, which it can use
to discover Alertmanager instances:
https://prometheus.io/docs/prometheus/latest/configuration/configuration.
Whatever mechanism we choose, it needs to work across all VPCs, GCE and GKE.

Prometheus instances ideally need to discover individual Alertmanager pods, not
a load balanced endpoint. This is part of the Prometheus alerting design: send
alerts to as many Alertmanager cluster members as possible, and the cluster will
de-duplicate the alert, minimizing the probability of missed alerts.

The only viable Prometheus SD configs are DNS, or static. We can't use
Kubernetes SD, because not all Prometheus instances are in the same Kubernetes
cluster as Alertmanager.

#### DNS SD

Deploy and configure
[external-dns](https://github.com/kubernetes-sigs/external-dns) to the ops
kuberndetes cluster, ensuring it is able to provision DNS records for a
subdomain zone of gitlab.net, e.g. `ops.gke.gitlab.net`. The exact DNS layout is
being discussed in
https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10556. There is
some existing work to deploy external-dns:
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/848.

Create a headless service for the Alertmanager deployment in ops Kubernetes,
annotated so that an A record is provisioned, resolving to pod IPs.

We would also have to delegate this subdomain zone from Cloudflare to GCP Cloud
DNS, in Terraform.

#### Static SD

Configure internal load balancers for each Alertmanager pod, and configure their
static private IPs at each Prometheus instance.

We would have to carry out a spike to be confident that we can maintain an
external HTTPS IAP load balancer across the service for human ingress, and also
an internal HTTP load balancer per pod.

### Network Access

The gprd, gstg, pre, and testbed are all peered with ops, with no overlapping
subnets. CI is not peered, but we probably don't want that (see above).

Currently, the GCE firewall in ops contains no Deny rules. We use the default
VPC, and have not modified the default-allow-internal rule, which allows all
TCP, UDP, and ICMP traffic from 10.128.0.0/9 to all nodes. That CIDR includes
all peered VPC subnets. In summary, we currently run a completely open internal
network inside ops, available to its peers. This means no work will need to be
done to make alertmanager pods available, across peering tunels but this
open-ness is something we might want to pull back in the future (unrelated to
this epic).

We use GKE's [VPC
native](https://cloud.google.com/kubernetes-engine/docs/how-to/alias-ips)
feature, which uses GCE subnet secondary ranges as the pod and service CIDRs,
allowing 2-way routing between GCE and GKE. This allows GKE pods to be
visible outside of the VPC through peering tunnels, although this will be
verified in a spike.

The current GCE alertmanager cluster is spread over nodes in production and ops,
and there is currently a single node gprd-GKE Alertmanager too. By meshing
these, and a new deployment in GKE ops, we can begin to re-point all Prometheus
instances to the new ops endpoints, before turning down the legacy nodes.

This is all being discussed in
https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10539.

CI is not like the others. The current gitlab-ci project's VPC is not currently
peered to ops, because we want to minimise the access granted to arbitrary code.
It doesn't appear able to send alerts at all, and CI alerts are sent via the
gprd prometheus nodes, which themselves scrape the CI prometheus instances over
the public internet (through strict firewall rules).

https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10374 is where
the plans for CI are being discussed. The current plan is to VPC-peer CI
projects with ops, using firewall rules to prevent the runners themselves
contacting any other infrastructure. This way, CI Prometheus instances, which
will be in a separate subnet to the runners, will be able to send alerts through
the peering tunnel.

### Preferred Approach

We are currently leaning towards the DNS for service discovery, for a few
reasons:

1. It allows us to make changes to the Alertmanager cluster without silently
   breaking connectivity from Prometheus instances.
1. The estimated complexity of each approach is roughly the same. The DNS
   approach is conceptually a bit more complex, but the implementation
   complexity of the static SD config is likely quite fiddly from both a GKE and
   terraform perspective, and might not be possible.
1. Cross-cluster DNS is something that could be quite useful in the future.

By running the Alertmanager cluster in the ops GKE, we could avoid peering
production with any non-prod VPC, and instead peer everything with ops. We would
first have to ensure that there is no other reason for staging-production
peering before removing that tunnel. This could mitigate most of the firewall
concerns described above.

This has potential implications for future network routing/peering/firewall
concerns.  If all we know about the Alertmanager IPs is that they are in the ops
cluster's pod CIDR, then we will have to be more liberal with our firewall
restrictions than if we knew a list of static IPs. Today, we do not restrict
private traffic at all, even across the peering tunnels, so this proposal does
not represent a loosening of firewall restrictions.

A potential downside to this approach is that we'd have to rely on the
reliability of the production - ops peering tunnel. We could measure it in
advance if we want to spend that effort on mitigating this risk.

## Alternative solutions

This section contains variants on the main proposal above.

### Static Alertmanager IPs

By using internal load balancers for each Alertmanager pod, as described in the
"Static SD" section, but also using DNS to discover each endpoint, we could
narrow the scope of the firewall access needed by Prometheus instances.

### Non-prod alertmanager

We could try to eliminate ops-nonprod peering by running one or more nonprod
Alertmanagers. We'd still likely have to peer "prod-like" environments together:
i.e. production and ops.

An advantage of this approach is that we could test out Alertmanager changes on
a nonprod cluster.

A disadvantage of this approach is that it is more complex than running one
Alertmanager.

### Federated Cluster DNS

We could look into replacing the default Kubernetes cluster DNS (kubeDNS) with
something else (e.g. CoreDNS), and federating our various clusters to provide
cross-cluster DNS, e.g. as described in
https://kubernetes.io/blog/2016/07/cross-cluster-services.

There are a lot of unknowns to this approach, so more exploration would have to
be done to flesh out this idea.

It's probably unworkable while we still have Prometheus instances in GCE. For
this reason we haven't given it much thought, in the interest of cutting scope.
The solution complexity is also likely quite high.

## Issues

This epic should be approximately ordered, and contains issue dependencies:
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/234.

## Follow-on work

Eventually we want to move all Prometheus and Thanos components into Kubernetes
clusters.
